	<header id="global">

		<div class="contain">
			<!-- main logo -->
			<a class="logo" href="/"><?php echo get_bloginfo('name'); ?></a>
			<!-- end main logo -->
			<a class="home" href="/">Back to Home</a>
			<nav>
				<ul>
					<li class="social"><a target="_blank" href="<?php the_field('twitter', 'option'); ?>" class="icon twitter">Twitter</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('facebook', 'option'); ?>" class="icon facebook">Facbeook</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('linkedin', 'option'); ?>" class="icon linkedin">Linkedin</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('youtube', 'option'); ?>" class="icon instagram">Instagram</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('google', 'option'); ?>" class="icon google">Google+</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('flickr', 'option'); ?>" class="icon flickr">Flickr</a></li>
                    <li><a href="/about-us/uli/">About Us</a></li>
                    <li class="last"><a href="<?php echo get_permalink( 40 ) ?>"><?php echo get_the_title( 40 ); ?> </a></li>
                    
					<li class="social search">
                        <form role="search" method="get" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                            <input type="text" class="search" name="s">
                            <a href="#" class="icon search" id="search">Search</a>
                        </form>
					</li>
					<li class="social">
						<a id="launch-report" href="#main-nav" class="icon explore">Open Menu</a>
					</li>
				</ul>
			</nav>

		</div>
	</header>