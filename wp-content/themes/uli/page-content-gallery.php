<?php
/*
Template Name: Content: Full width_Gallery
*/

	get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<!-- section header -->
		<header class="sub-header alternative">
			<h1 class="section-title"><?php the_title(); ?></h1>
			<span class="icon share share-this">Share</span>
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="full">
			
				<div class="content">
          
					<?php the_content(); ?>
				</div>
							

			<!-- end column 1 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
			<style>
			p, h3
			{
					font-family:"Gotham A","Gotham B",sans-serif;
					color:#017573;
			}
			p:hover, h3:hover
			{
					color:#8FB14c;
			}
				.info h3
				{
					font-size:32px;
					margin:0px;
					padding:0px;
					font-weight:bold;
				}
				.side p
				{
					font-size:15px;
					font-weight:bold;
					margin-top:5px;
					margin-left:5px;
					padding:0px;
					text-align:left;
					line-height:12.5px;
				}
				.info, .side
				{
						float:left;
				}
				.img
				{
						width:221px;
						height:71px;

				}
				.content_2
				{
					width:85%;
					margin:0px auto;
				}
				.content_2 p
				{
					width:267px;
					font-size:12.5px;
				}
				.side p
				{
					width:120px;
				}
				.info_side
				{
					margin-left:17.5%;
				}
				.inner_content_2
				{
					float:left;
					margin:1%;
					width:280px;
					height:250px;
				}
				.money
				{	
					margin-top:-10px;
					font-size:15px;
				}
				.inner_font
				{
					font-weight:bold;
					font-size:25px;
				}
				@media only screen and (max-width: 900px)
				{
				.content_2
				{
					width:100%
				}
				}
				@media only screen and (max-width: 520px)
				{
				.content_2
				{
					width:100%
				}
				.inner_content_2
				{
					float:left;
					margin:0px;
				}
				}
			</style>