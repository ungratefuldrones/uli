
	<?php
		if ( is_front_page() ) {
			include_once('home-footer.php');
		}
	?>
	
	<section class="share-overlay">
        <a class="close" href="" ><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/icon-overlay-close.png"></a>
        <div class="share-inner">
            <div class="title">Share</div>
            <!-- Go to www.addthis.com/dashboard to customize your tools -->
            <?php 
            	// echo get_the_title();
            ?>
			<div class="addthis_sharing_toolbox" data-title="<?php echo get_the_title(); ?>" data-url="<?php echo get_permalink(); ?>"></div>
        </div>
    </section>
    
	<!-- Go to www.addthis.com/dashboard to customize your tools -->
	<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-54f4bc800802c851" async="async"></script>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script src="<?php bloginfo('template_url'); ?>/assets/js/script.js"></script>
	<script>try{Typekit.load();}catch(e){}</script>
	
	<?php wp_footer(); ?>
	</body>
</html>