/*! uliannualreport 2015-03-25 */
$(document).ready(function(){

	if($('body.home')){
		videoHomepage();
		leadershipMessage();
	}
	
	mainNavigation();
	innerPageBackground();

	/* global sharing */

	var $shareOverlay = $('.share-overlay');

	// open
	$('.share-this').click(function() {
		$shareOverlay.fadeIn(500);
		updateAddthis();
	});

	// close
	$shareOverlay.find('.close').click(function(e) {
		e.preventDefault();
		$shareOverlay.fadeOut(500);
	});


	/* story landing page height */

	$('.story-landing .top-image').height( $(window).height() - $('header#global').outerHeight() );

	/* story landing page content scroll */

	$('.scroll-down').click(function() {
	    $('html, body').animate({
	        scrollTop: $('.content-scroll').offset().top
	    }, 1000);
	});

	/* homepage mobile toggle button */

	$('.click-nav').click(function() {
		$('.brand-2.cta.icon.explore').trigger('click');
	});

	/* toggles on 'Annual Fund' & 'Foundation Govenors' page */

	$('.toggles-wrap .toggle .top').click(function() {
		var $thisToggle = $(this).closest('.toggle');
		if ( $thisToggle.hasClass('open') ) {
			$thisToggle.removeClass('open').find('.bottom').slideUp(300);
		}
		else {
			$('.toggle.open').removeClass('open').find('.bottom').slideUp(300);
			$thisToggle.addClass('open').find('.bottom').slideDown(300);
		}
	});

});

function updateAddthis() {
    
} 

function videoHomepage() {
	if ( $(window).width() > 1024 ) {
		function animateScene() {
			$('.hidden').each(function(){
				var hiddenClass = $('body').find().attr('class="hidden"');
				$(this).addClass('show');
			});
		};
		var runAtTime = function(handler, time) {
		     var wrapped = function() {
		         if(this.currentTime >= time) {
		             $(this).off('timeupdate', wrapped);
		             return handler.apply(this, arguments);
		        }
		     }
		    return wrapped;
		};
		$('#full-screen-video').on('timeupdate', runAtTime(animateScene, .7));
	}
	else {
		$('.hidden').each(function(){
			$(this).addClass('show');
		});
	}
 }

function leadershipMessage() {
	var $open_overlay_button = $('a#launch-leadership').attr('href','#leadership-message'),
		$close = $('.icon.overlay-close'),
		$video_bg = $('section#video'),
		$play_video = $('.video-overlay-bg, .video-overlay'),
		$stop_video = $('.leadership-video .x'),
		$leadership_message = $('#leadership-message'),
		$leader_link = $('.leader-link'),
		window_height = $(window).height() - 40,
		router = {
			routes : {
				'#leadership-message' : 'open_overlay',
				'#leadership-video' : 'play_video',
				'#leader' : 'scroll_to_leader',
				'default' : 'close_overlay',
			},
			is_overlay_open : false,
			set_route : function(hash) {
				window.location.hash = String(hash);
			},
			open_overlay : function() {
				$('#video-wrapper').addClass('leadership-open');
				leadership_video.pause();
				$leadership_message.addClass('active');
				$(window).on('resize.leadership_video', function() {
					leadership_video.resize();
				});
				if (this.is_overlay_open) {
					$('#leadership-message').animate({
						scrollTop: 0
					}, 500);
				}
				this.is_overlay_open = true;
				router.set_route('leadership-message');
			},
			close_overlay : function() {
				$('#video-wrapper').removeClass('leadership-open');
				if (this.is_overlay_open) {
					leadership_video.pause();
					$leadership_message.removeClass('active');
					$(window).off('resize.leadership_video');
					this.is_overlay_open = false;
				}
			},
			play_video : function() {
				if (!this.is_overlay_open) {
					this.open_overlay();
				}
				leadership_video.play();
			},
			pause_video : function() {
				leadership_video.pause();
			},
			scroll_to_leader : function(hash) {
				if (!this.is_overlay_open) {
					this.open_overlay();
					setTimeout(function() {
						$('#leadership-message').animate({
							scrollTop: $('.' + hash).offset().top
						}, 500);
					}, 300);
				} else {
					$('#leadership-message').animate({
						scrollTop: $('.' + hash).offset().top
					}, 500);
				}
				
			}
		},
		route = function() {
			var hash = window.location.hash;
			var found = false;
			Object.keys(router.routes).forEach(function (key) { 
				if (hash.search(key) > -1 && !found) {  // dirty, should probably switch to loop that can break instead of check for found.
					// run the function. http://stackoverflow.com/questions/912596/how-to-turn-a-string-into-a-javascript-function-call
					var fn = router[router.routes[key]];
					if (typeof fn === 'function') {
						fn.apply(router, [hash.substring(1)]);
					}					
					found = true;

				}
			});
			if (!found) {
				var fn = router[router.routes['default']];
				if (typeof fn === 'function') {
					fn.apply(router, [hash.substring(1)]);
				}
			}
		},
		leadership_video = {
			playing : false,
			play : function() {
				if ( $(window).width() > 1024 ) {
					if (typeof player !== 'undefined') { // player is loaded async
						player.playVideo();
					} else {
						window.queue_player = true; // if this flag is set, the video will start to play when it loads, see function onPlayerReady
					}
				}
				this.playing = true;
				$('.leadership-video').fadeIn(500);
				$('.video-overlay, .video-overlay-bg, .helper, .overlay-close').addClass('hidden');
			},
			pause : function() {
				if (this.playing && typeof player !== 'undefined') {
					player.pauseVideo();
					$('.video-overlay, .video-overlay-bg, .helper, .overlay-close').removeClass('hidden');
				}

			},
			resize : function() {
				height = $(window).height();
				width = $(window).width();
				$('#leadership-video-player').attr('width', width).attr('height', height);
			}
		};

	$('#leadership-message .leaders').on('click', function(e) {
		e.stopPropagation();
	});

	// Leadership Video
	$play_video.on('click', function(e) {
		e.preventDefault();
		router.set_route('leadership-video');
	});
	$stop_video.on('click', function(e) {
		e.preventDefault();
		router.set_route('leadership-message');
		$('.leadership-video').removeClass('fixed-pos');
	});
	$close.on('click', function(e) {
		e.preventDefault();
		router.set_route('');
	});
	$leader_link.on('click', function(e){
		e.preventDefault();
		router.set_route($(e.currentTarget).data('hash'));
	});

	$(window).on('hashchange', function() {
		route();
	});
	$(window).trigger('hashchange');

	// set height of video overlay
	if ( $(window).width() > 1024 ) {
		$video_bg.height(window_height);
	}
	else {
		var $vidHeight = $(window).height();
		$('#leadership-message #video .leadership-video').height($vidHeight).addClass('fixed-pos');
		$('#leadership-message #leadership-video-player-mobile').attr('height', $vidHeight-50);
	}


	$(window).resize(function(){
		window_height = $(window).height();
		if ( $(window).width() > 1024 ) {
			$video_bg.height(window_height);
		}
		else {
			var $vidHeight = $(window).height();
			$('#leadership-message #video .leadership-video').height($vidHeight).addClass('fixed-pos');
		}
	});

	/* scroll to top */

	$('.back-to-top').click(function(e) {
		e.preventDefault();
		$('#leadership-message').animate({
			scrollTop: 0
		}, 500);
		router.set_route('leadership-message');
	});

	

	

}

function mainNavigation() {
	var button = $('a#launch-report').attr('href','#main-nav'),
		close = $('a#close-main-nav').attr('href','#main-nav'),
		search = $('#search');

	//this button activates a div with an id the same href value
	button.click(function(){

		$('body').addClass('menu-overflow');

		//locate div with href hash
		var sectionID = $(this).attr('href');
		
		//show div with href hash
		$(sectionID).addClass('active');

		animation();

		return false;
	});

	//this button deactivates the div
	close.click(function(){

		$('body').removeClass('menu-overflow');

		//locate div with href hash
		var sectionID = $(this).attr('href');
		
		//show div with href hash
		$(sectionID).removeClass('active');
	});

	//this button deactivates the div
	search.click(function(){

		$('header li.search, header li.search input').toggleClass('active');
		$('header li.search input').focus();

	});

	function animation() {
		//var item = $('#main-nav li');

		var divs = $( '#main-nav nav li' ),
			index = 0;

		var delay = setInterval( function(){
		  if ( index <= divs.length ){
		    $( divs[ index ] ).addClass( 'active' );
		    index += 1;
		  }else{
		    clearInterval( delay );
		  }
		}, 40 );
	}

	function navHeight() {
		var header = $('#main-nav header'),
			nav = $('#main-nav nav'),
			headerHeight = header.height(),
			browserHeight = $(window).height();

		nav.height((browserHeight - headerHeight)-25);

	} navHeight();

	$(window).resize(function(){
		navHeight();
	});
}

function innerPageBackground() {
	function pageHeight() {
		var header = $('#global'),
			pageWrapper = $('#page-wrapper'),
			headerHeight = header.outerHeight(),
			browserHeight = $(window).height();
			pageWrapper.height(browserHeight - headerHeight);
	} pageHeight();

	$(window).resize(function(){
		pageHeight();
	});
}



// youtube video player code, adapted from https://developers.google.com/youtube/iframe_api_reference

// 2. This code loads the IFrame Player API code asynchronously.
var tag = document.createElement('script');

tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

// 3. This function creates an <iframe> (and YouTube player)
//    after the API code downloads.
var player,
	height = $(window).height(),
	width = $(window).width(),
	videoId = 'Yq9woKDsiag',
	videoPlayerId = 'leadership-video-player'
;
function onYouTubeIframeAPIReady() {
	player = new YT.Player(videoPlayerId, {
		height: height,
		width: width,
		videoId: videoId,
		events: {
			'onReady': onPlayerReady,
			'onStateChange': onPlayerStateChange
		}
	});
}

// 4. The API will call this function when the video player is ready.
function onPlayerReady(event) {
	if (window.queue_player) {
		event.target.playVideo();
	}
}

// 5. The API calls this function when the player's state changes.
//    The function indicates that when playing a video (state=1),
//    the player should play for six seconds and then stop.
function onPlayerStateChange(event) {
	// this has to be defined, not used for this site.
}

$( ".story-landing .col-2.sidebar li a" ).on("click touchstart", function( e ) {
	e.preventDefault();
	var $parent = $(this).parents('li') 
		,params= $.param($parent.data())
		,$contentArea = $('.content-area')
		,$title = $contentArea.find('h2')
		,$content= $contentArea.find('.entry-content')
	;
	
	$parent.addClass('active').siblings().removeClass('active');	

    $.ajax({
          type: 'POST',   
          url: '/wp-admin/admin-ajax.php',
          data: {
              action: 'hfc-fetch-post',
              data: params
          },
          dataType: 'json',
          success: function(data) {
          	$contentArea.fadeOut(500, function() {
          		$title.html(data[0].post_title);
	        	$content.html(data[0].post_content);
	        	$contentArea.fadeIn(500);
          	});
   			
        }
    });
});

