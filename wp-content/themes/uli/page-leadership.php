<?php 
/*
Template Name: Leadership
*/

get_header();
?>

<!-- home content -->
<article class="home-content disabled" id="leadership-message">
	<!-- close button -->
	<a class="close icon" id="close-home-content" href="#leadership-message">X</a>
	<!-- end close button -->
	<div class="contain">
		<!-- header -->
		<header class="sub-header">
			<h1 class="section-title">test</h1>
			<span class="icon share share-this">Share</span>
			<nav class="sub-nav">
				<ul>
					<li><a href="#" class="active">UlI Chairman</a></li>
					<li><a href="#">UlI CEO</a></li>
					<li><a href="#">UlI Foundation Chairman</a></li>
				</ul>
			</nav>
		</header>
		<!-- end header -->

		<article class="content">
			<img class="full" src="<?php bloginfo('template_url'); ?>/assets/images/leadership-placeholder.jpg" alt="Leadership">
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla erat ut lacus finibus elementum. Aenean facilisis, metus imperdiet hendrerit tempus, purus felis imperdiet quam, non interdum nisi leo vulputate augue. Morbi maximus condimentum lorem, eget viverra odio molestie in. Mauris dignissim, neque eget eleifend vulputate, dui orci euismod ex, id tincidunt massa enim ac felis. Etiam sit amet leo eget metus commodo dictum quis consequat augue. Nulla dignissim in quam sit amet efficitur. Integer sit amet tellus nec nulla finibus volutpat in at quam. Sed congue ex elit, et consequat augue aliquam in. Mauris suscipit sapien orci, et aliquam justo tristique nec.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla erat ut lacus finibus elementum. Aenean facilisis, metus imperdiet hendrerit tempus, purus felis imperdiet quam, non interdum nisi leo vulputate augue. Morbi maximus condimentum lorem, eget viverra odio molestie in. Mauris dignissim, neque eget eleifend vulputate, dui orci euismod ex, id tincidunt massa enim ac felis. Etiam sit amet leo eget metus commodo dictum quis consequat augue. Nulla dignissim in quam sit amet efficitur. Integer sit amet tellus nec nulla finibus volutpat in at quam. Sed congue ex elit, et consequat augue aliquam in. Mauris suscipit sapien orci, et aliquam justo tristique nec.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla erat ut lacus finibus elementum. Aenean facilisis, metus imperdiet hendrerit tempus, purus felis imperdiet quam, non interdum nisi leo vulputate augue. Morbi maximus condimentum lorem, eget viverra odio molestie in. Mauris dignissim, neque eget eleifend vulputate, dui orci euismod ex, id tincidunt massa enim ac felis. Etiam sit amet leo eget metus commodo dictum quis consequat augue. Nulla dignissim in quam sit amet efficitur. Integer sit amet tellus nec nulla finibus volutpat in at quam. Sed congue ex elit, et consequat augue aliquam in. Mauris suscipit sapien orci, et aliquam justo tristique nec.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam fringilla erat ut lacus finibus elementum. Aenean facilisis, metus imperdiet hendrerit tempus, purus felis imperdiet quam, non interdum nisi leo vulputate augue. Morbi maximus condimentum lorem, eget viverra odio molestie in. Mauris dignissim, neque eget eleifend vulputate, dui orci euismod ex, id tincidunt massa enim ac felis. Etiam sit amet leo eget metus commodo dictum quis consequat augue. Nulla dignissim in quam sit amet efficitur. Integer sit amet tellus nec nulla finibus volutpat in at quam. Sed congue ex elit, et consequat augue aliquam in. Mauris suscipit sapien orci, et aliquam justo tristique nec.</p>
		</article>

	</div>
</article>
<!-- end home content -->

<?php get_footer(); ?>