<?php
/*
Template Name: Content: Full width
*/

get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover; background-attachment: fixed;">
    <div class="contain">
        <!-- section header -->
        <header class="sub-header alternative">
            <h1 class="section-title">Search</h1>
        </header>
        <!-- end section header -->

        <ul>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <!-- section header -->
                <li>
                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>

                    <p>
                        <a href="<?php the_permalink(); ?>">
                            <?php echo substr(strip_tags($post->post_content), 0, 300)." ... ";?>
                        </a>
                    </p>

                </li>
            <?php endwhile;
            else : ?>
                <h1 class="section-title">0 Results have been found</h1>
            <?php endif; ?>
        </ul>
    </div>
</div>

<?php get_footer(); ?>