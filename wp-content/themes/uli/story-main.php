<?php 


	get_header(); 
	
	// fetch chapters
	$chapters = get_field('main_menu_items', 'option');

	$numbers = array('one', 'two', 'three', 'four', 'five', 'six');

	// fetch Stories home ID
	$story_home = get_page_by_title( 'Stories', OBJECT, 'page' );
	
	$is_leaf = $story_home->ID != $post->post_parent ? true : false;

	$seq_id =  $is_leaf ? $post->post_parent : $post->ID;

	// fetch stories
	$stories = get_pages('child_of='. $seq_id .'&depth=1&post_type=page');

	// find chapter offset in main sequence  
	$chapter_seq = $numbers[key(array_filter( $chapters, function( $k ) use ($seq_id) {
					return $k->ID == $seq_id;
	}))]; 

	// find story in sequence, or if chapter page 
	$story_seq =  !$is_leaf ?  0 : key(array_filter( $stories, function( $k ) use ($post) {
										return $k->ID == $post->ID;})); 			
	$chapter = get_page( $seq_id );

	$story = $stories[ $story_seq ];

	// fetch background image 
	$bg_url = get_field('story_background_image', $seq_id);


?>
<section class="story-landing" data-uri="<?php echo get_page_uri( $chapter->ID ); ?>">
	<section class="top-image" style="background-image: url('<?php echo $bg_url; ?>');">
		<span class="icon share share-this">Share</span>
		<div class="fade"></div>
		<div class="scroll-down"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/scroll-down.png"></div>
		<div class="inner">
			<div class="chapter">ULI Priority <?php echo $chapter_seq; ?></div>
			<h1><?php echo $chapter->post_title; ?></h1>
			<?php echo apply_filters( 'the_content', $chapter->post_content); ?>
		</div>
	</section>
		<div class="row content-scroll">
			<div class="col-1">
				<section class="content-area">
					<?php if ( $story ) : ?>
						<h2><?php echo $story->post_title; ?></h2>
						<div class="entry-content">
							<?php echo apply_filters( 'the_content', $story->post_content ); ?>
						</div>
					<?php endif; ?>
				</section>
			</div>
		<div class="col-2 sidebar">
			<section class="nav-sidebar">
				<span>Issues in this Priority</span>
				<ul>
					<?php if ( $stories ) : ?>
					<?php foreach ( $stories as $i=>$j ) : ?>
						<li class="<?php echo $i == $story_seq ? 'active': ''; ?>" data-p="<?php echo $j->ID; ?>" data-post_type="page" >
							<a href="<?php echo get_permalink($j->ID);?>">
								<?php echo $j->post_title; ?>
							</a>
						</li>
					<?php endforeach; endif; ?>
				</ul>
			</section>
			<span class="icon share share-this">Share This Issue</span>
		</div>
	</section>
</section>

<?php get_footer(); ?>