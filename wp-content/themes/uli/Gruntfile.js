module.exports = function(grunt) {
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    fileName: '<%= grunt.config.get("pkg").name.toLowerCase().replace(/ /g, "-") %>',
    jsFiles: [
			'assets/js/*.js'
    ],
    cssFiles: [
      'assets/css/*.css'
    ],
    sassFiles: ['sass/styles.scss'],
    distDir: 'assets/dist/',
    sass: {
      uncompressed: {
        options: {
          compress: false
        },
        files: {
          'assets/css/styles.css': '<%= sassFiles %>'
        }
      }
    },
    concat: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
        stripBanners: true
      },
      js: {
        src: '<%= jsFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.js'
      },
      css: {
        src: '<%= cssFiles %>',
        dest: '<%= distDir %><%= fileName %>.all.css'
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= fileName %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: '<%= distDir %><%= fileName  %>.all.js',
        dest: '<%= distDir %><%= fileName %>.all.min.js'
      }
    },
    watch: {
      js: {
        files: '<%= jsFiles %>',
        tasks: ['concat:js']
      },
      sass: {
        files: '<%= sassFiles %>',
        tasks: ['sass']
      },
      css: {
		    files: '<%= cssFiles %>',
		    tasks: ['concat:css']
      }
    }
  });
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  return grunt.registerTask('default', ['sass', 'concat', 'watch']);
};
