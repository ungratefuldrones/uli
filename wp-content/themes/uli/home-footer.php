
	<footer class="home-footer hidden">
		<div class="contain">
			<!-- copyright -->
			<div class="copyright">
				&copy; 2015 Urban Land Institute. All Rights Reserved.
			</div>
			<!-- end copyright -->

			<!-- download report 
			<a class="block icon download" href="#">
				Download Report
			</a>
			end download report -->
		</div>
	</footer>
