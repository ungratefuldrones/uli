<?php
/*
Template Name: Content: Full width
*/

	get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<!-- section header -->
		<header class="sub-header alternative">
			<h1 class="section-title"><?php the_title(); ?></h1>
			<span class="icon share share-this">Share</span>
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="full">
				<nav class="sub-nav">
					<ul>
                        <?php
                        $parent = get_post_ancestors($post->ID);
                        $grand_father = $parent[count($parent)-1];
                        wp_list_pages(array(
                            'title_li' => "",
                            'child_of' => $grand_father,
                            // Only show one level of hierarchy
                            'depth' => 1
                        ));
                        echo "<br>";
                        wp_list_pages(array(
                            'title_li' => "",
                            'child_of' => $post->post_parent,
                            // Only show one level of hierarchy
                            'depth' => 1
                        ));

                        ?>
					</ul>
				</nav>

				<div class="content">
					<?php the_content(); ?>
				</div>
			</div>
			<!-- end column 1 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>