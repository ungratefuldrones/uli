<?php 
/*
Template Name: Video Snippets
*/

	get_header();
?>

<?php if( get_field('videos_background_image') ): ?>
<div id="page-wrapper" style="background: url('<?php the_field('videos_background_image'); ?>'); background-size: cover; background-attachment: fixed;">
<?php endif; ?>
	<div class="contain">
		<div class="wrapper">
				<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
				<!-- section header -->
				<header class="sub-header alternative">
					<h1 class="section-title"><?php the_title(); ?></h1>
					<span class="icon share share-this">Share</span>
				</header>
				<!-- end section header -->

				<!-- row -->
				<div class="row">
					<!-- column 1 -->
					<div class="col-1">

						<ul class="snippets">
							<?php
								$parent_id = $posts[0]->ID;
								$args=array(
								  'post_parent' => $parent_id,
								  'post_type' => 'page',
								  'post_status' => 'publish',
								  'orderby' => 'menu_order',
								  'order' => 'ASC',
								  'posts_per_page' => -1,
								  'caller_get_posts'=> 1
								);
								$my_query = null;
								$my_query = new WP_Query($args);
								if( $my_query->have_posts() ) {
								  while ($my_query->have_posts()) : $my_query->the_post(); ?>

								<?php
								$image_id = get_post_meta( get_the_ID(), 'video_detail_main_photo', true );
								//$sub_title = get_post_meta( get_theID()), 'highlights_sub_title');

								if ( ! $image_id ) {
								    //echo "<p>Image ID not true. It is:</p><pre>"; var_dump( $image_id ); echo "</pre>";
								} else if ( $image_id ) {
								    # The image id was greater than zero - if none were set it would currently be false as per the 3rd parameter of get_post_meta
								    # The next line tries to grab an image with that id in thumbnail size
								    $image_data = wp_get_attachment_image_src( $image_id, 'full' );
								    if ( ! is_array( $image_data ) ) {
								        echo "<p>Image data not an array! It is:</p><pre>"; var_dump( $image_data ); echo "</pre>";
								    } else if ( is_array( $image_data ) ) {
								        # The next line grabs the url, which is stored in the first element of the array
								        $image_url = $image_data[0];

								        # the next three lines just grab post information
								        $post_id = get_the_ID();
								        $post_title = esc_attr( get_the_title() );
								        $post_link = get_permalink();
								        $sub_title = get_post_meta( $post_id,'video_detail_length', true );
								        $short_paragraph = get_the_content();
								        $detail = substr(strip_tags($short_paragraph), 0, 250)." ... ";
										echo "<li>
													<a href=".$post_link." class=\"photo icon video\" style=\"background-image: url($image_url)\"></a>
													<div class=\"column\">
														<h2 class=\"text\"><a href=".$post_link.">$post_title</a></h2> 
														<h3>$sub_title</h3>
														<p>$detail</p>
													</div>
											  </li>";
								    }
								}
							?>
							<?php   endwhile;
								}
								wp_reset_query();  // Restore global post data stomped by the_post().
							?>
						</ul>

						</div>
						<!-- end column 1 -->
						<!-- column 2 -->
						<div class="col-2 sidebar">
							<!-- sidebar nav -->

							<nav class="sub-nav">
								<ul>
								  <?php

			                      if(count(get_children($post->ID)) > 0) {
			                          wp_list_pages(array(
			                              'title_li' => "",
			                              'child_of' => $post->post_parent,
			                              // Only show one level of hierarchy
			                              'depth' => 0
			                          ));
			                      }else{

			                          $parent = get_post_ancestors($post->ID);
			                          $grand_father = $parent[count($parent)-1];
			                          wp_list_pages(array(
			                              'title_li' => "",
			                              'child_of' => $grand_father,
			                              // Only show one level of hierarchy
			                              'depth' => 2
			                          ));
			                      }
								   ?>
								</ul>
							</nav>
							<!-- end sidebar nav -->
						</div>
						<!-- end column 2 -->
					</div>
					<!-- end row -->

			<?php endwhile; else : ?>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php get_footer(); ?>