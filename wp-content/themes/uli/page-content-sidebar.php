<?php 
/*
Template Name: Content: 2 Column With Sidebar
*/

	get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover; background-attachment: fixed;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
		<!-- section header -->
		<header class="sub-header alternative">
      <?php $parentTitle = get_the_title($post->post_parent); ?>
      <h1 class="section-title">
        <?php if ( $parentTitle == 'Company' ) {
          echo get_the_title($post->ID);
        } else {
  		    echo $parentTitle;
        } ?>
      </h1>
			<span class="icon share share-this">Share</span>
      
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="col-1">
				<nav class="sub-nav">
					<ul>
						<?php
						// Globalize the $post variable;
						// probably already available in this context, but just in case...
                        //echo "This post has: ".count(get_children($post->ID))." children <br>";
                        global $post;
                        if($post->post_parent == 31){
                            wp_list_pages(array(
                                'title_li' => "",
                                'child_of' => get_post_ancestors($post->ID)[1],
                                // Only show one level of hierarchy
                                'depth' => 1
                            ));
                        }
                        if($post->ID == 31){
                            wp_list_pages(array(
                                'title_li' => "",
                                'child_of' => $post->post_parent,
                                // Only show one level of hierarchy
                                'depth' => 1
                            ));
                        }
                        $title = get_the_title($post->post_parent);
                        echo "</ul><ul class=\"sub-sub-nav\">";
                        if(count(get_children($post->ID)) > 0){

                            wp_list_pages( array(
                                'title_li' => "",
                                'child_of' => $post->ID,
                                // Only show one level of hierarchy
                                'depth' => 1
                            ) );
                        }else{
                            if(count(get_post_ancestors($post->ID)) > 1){
                                wp_list_pages(array(
                                    'title_li' => "",
                                    'child_of' => $post->post_parent,
                                    // Only show one level of hierarchy
                                    'depth' => 1
                                ));
                            }

                        }
                        ?>
						
					</ul>
				</nav>
				<div class="content">
          <?php if ( the_parent_slug($post) == 'videos' ) { ?>
            <iframe width="100%" height="450px" src="https://www.youtube.com/embed/<?php echo get_field('video_detail_video_url'); ?>" frameborder="0" allowfullscreen></iframe>
          <?php } ?>
					<?php the_content(); ?>
				</div>
							</div>
			<!-- end column 1 -->
			<!-- column 2 -->
			<div class="col-2 sidebar">
				<!-- sidebar nav -->
                <!--<style>
                    #page-wrapper .sidebar nav.sub-nav li ul {
                        display: block !important;
                    }
                </style>-->

				<nav class="sub-nav">
					<ul>
					  <?php
                            if($post->ID == 31){
                                wp_list_pages(array(
                                    'title_li' => "",
                                    'child_of' => get_post_ancestors($post->ID)[1],
                                    // Only show one level of hierarchy
                                    'depth' => 1
                                ));
                            }elseif(count(get_children($post->ID)) > 0) {
                              wp_list_pages(array(
                                  'title_li' => "",
                                  'child_of' => $post->post_parent,
                                  // Only show one level of hierarchy
                                  'depth' => 0
                              ));
                            }else{

                              $parent = get_post_ancestors($post->ID);
                              $grand_father = $parent[count($parent)-1];
                              wp_list_pages(array(
                                  'title_li' => "",
                                  'child_of' => $grand_father,
                                  // Only show one level of hierarchy
                                  'depth' => 2
                              ));
                            }
					  //wp_list_pages('title_li=&child_of='.$post->ID.''); ?>
					</ul>
				</nav>
				<!-- end sidebar nav -->
			</div>
			<!-- end column 2 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>