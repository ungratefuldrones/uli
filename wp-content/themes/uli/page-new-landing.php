<?php get_header(); ?>

<section class="story-landing">
	<section class="top-image" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/bg-people.jpg');">
		<div class="fade"></div>
		<div class="scroll-down"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/scroll-down.png"></div>
		<div class="inner">
			<div class="chapter">ULI Priority One</div>
			<h1>Advising Communities in Need</h1>
			<p>ULI’s Advisory Services panels provide strategic advice and practical solutions to communities facing urban development challenges. From citywide master plans to individual sites, the panels bring together the best and brightest from ULI’s diverse membership to offer candid and impartial advice.</p>
		</div>
	</section>
	<div class="row content-scroll">
		<div class="col-1">
			<section class="content-area">
				<h2>Building Healthy Places Across Colorado</h2>
				<p>The mission dovetails with ULI’s Building Healthy Places Initiative, which seeks to promote health in projects and places across the globe. The foundation sought the thought leadership of ULI on the question of how changes to the built environment, upgrades in infrastructure, and community programming can reduce obesity rates and chronic conditions like diabetes and cardiovascular disease.</p>
				<p>“Historically, it’s been easier to focus on individual behavior change, but the more we worked in this area, we came to know that the built environment plays a huge role in people’s ability to change their lifestyle,” says Hillary Fulton, the foundation’s senior program officer, Healthy Living.</p>
				<p>As a result of the collaboration between ULI and the Colorado Health Foundation, three Advisory Services panels were held, offering assistance to three communities—one rural, one suburban, and one urban—on how to position themselves as healthy places to live and work. The communities were selected through a competition sponsored by the foundation. Following the panels, each community received a $1 million grant from the foundation and technical assistance from Progressive Urban Management Associates, a Denver-based consulting firm, to implement the recommendations.</p>
				<p>“ULI’s partnership with the Colorado Health Foundation was the perfect vehicle for our members to contribute their land use expertise to three unique Colorado communities searching for strategies and solutions to enhance public health and to create long-term economic viability through the built environment,” says Ed McMahon, ULI senior resident fellow, who chaired the three panels.</p>
				<p>The communities selected were Arvada (suburban), Lamar (rural), and Westwood (urban). The panel’s recommendations and the progress each has made in making the healthy choice the easy choice follow.
				Arvada.</p>
			</section>
		</div>
		<div class="col-2 sidebar">
			<section class="nav-sidebar">
				<span>Stories in this Chapter</span>
				<ul>
					<li><a href="">Cook County Land Bank Off to a Strong Start</a></li>
					<li class="active"><a href="">Building Healthy Places Across Colorado</a></li>
				</ul>
			</section>
		</div>
	</section>
</section>

<?php get_footer(); ?>