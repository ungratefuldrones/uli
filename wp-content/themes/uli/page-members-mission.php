<?php 
/*
Template Name: Members on a Mission
*/

	get_header();
?>

<?php $custom_fields = get_post_custom(); ?>

<div id="page-wrapper" class="members-home" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover; background-attachment: fixed;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

            <?php
                if(function_exists('bcn_display'))
                {
                    bcn_display();
                }
            ?>
            
		<!-- section header -->
		<header class="sub-header alternative">
            <div class="members-breadcrumb">
                <a href="/about-us/videos/">Members on a Mission</a> 
            </div>
			<span class="icon share share-this">Share</span>
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="col-1">

				<div class="content">
                    <h2><?php the_title(); ?></h2>
                    <?php if ( the_parent_slug($post) == 'videos' ) { ?>
		            <iframe width="100%" height="450px" src="https://www.youtube.com/embed/<?php echo get_field('video_detail_video_url'); ?>" frameborder="0" allowfullscreen></iframe>
		          	<?php } ?>
					<?php the_content(); ?>         
                </div>      
			</div>
			<!-- end column 1 -->
            
			<!-- column 2 -->
			<div class="col-2 sidebar">
				<!-- sidebar nav -->
				<nav class="sub-nav">
					<ul>
                        <?php
                            
                            $parent_page_id = 42;

                            wp_list_pages(array(
                                'title_li' => "",
                                'child_of' => $parent_page_id,
                                'depth'    => 2,
                            ));

                        ?>
					</ul>

				</nav>
				<!-- end sidebar nav -->
			</div>
			<!-- end column 2 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>