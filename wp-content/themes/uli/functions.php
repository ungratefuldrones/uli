<?php

function the_breadcrumb() {
    if (!is_home()) {
        echo '<a href="';
        echo get_option('home');
        echo '">';
        bloginfo('name');
        echo "</a>  ";
        if (is_category() || is_single()) {
            the_category('title_li=');
            if (is_single()) {
                echo "  ";
                the_title();
            }
        } elseif (is_page()) {
            echo the_title();
        }
    }
}

// add the options pages
if( function_exists('acf_add_options_page') ) {     
    acf_add_options_page(array(
        'page_title'    => 'Options',
        'menu_title'    => 'Options',
        'menu_slug'     => 'options',
        'capability'    => 'edit_posts',
        'redirect'  => false
    ));
    acf_add_options_sub_page(array(
        'page_title'    => 'Promo Box',
        'menu_title'    => 'Promo Box',
        'parent_slug'   => 'options',
    ));
}


function the_parent_slug() {
  global $post;
  if($post->post_parent == 0) return '';
  $post_data = get_post($post->post_parent);
  return $post_data->post_name;
}

function hfc_fetch_post() { 
    global $wpdb;
    
    $args = array(); parse_str($_POST['data'], $args);
    $return = array();

    $return_args = array(
        'post_title' 
        ,'post_content'
    );
    
    $return_formatting = array(
        'post_title' => 'the_title'
        ,'post_content' => 'the_content'
    );

    $results = new WP_Query($args);

    foreach( $results->posts as $result ){
        $_post = array();
        foreach ( $result as $k=>$v ) {
            if ( in_array($k, $return_args ) ){       
                $_post[$k] = call_user_func_array('apply_filters', array($return_formatting[$k], $v));
            }
        }
        $return[] = $_post;
    }
    wp_send_json($return);
    wp_die();   
}

add_action( 'wp_ajax_nopriv_hfc-fetch-post', 'hfc_fetch_post' );
add_action( 'wp_ajax_hfc-fetch-post', 'hfc_fetch_post' );

?>