<?php 
/*
Template Name: Financial
*/

	get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover; background-attachment: fixed;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		
		<!-- section header -->
		<header class="sub-header alternative">
			<h1 class="section-title">Financial Performance</h1>
			<span class="icon share share-this">Share</span>
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="full tabs">
				<nav class="sub-nav tabs">
					<ul class="border">
                        <?php
                        $parent = get_post_ancestors($post->ID);
                        $grand_father = $parent[count($parent)-1];
                        wp_list_pages(array(
                            'title_li' => "",
                            'child_of' => $grand_father,
                            // Only show one level of hierarchy
                            'depth' => 1
                        ));
                        echo "</ul><ul class=\"tabs\">";
                        wp_list_pages(array(
                            'title_li' => "",
                            'child_of' => $post->post_parent,
                            // Only show one level of hierarchy
                            'depth' => 1
                        ));

                        ?>
					</ul>
				</nav>

				<div class="tabs-content">
					<?php the_content(); ?>
				</div>
			</div>
			<!-- end column 1 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>