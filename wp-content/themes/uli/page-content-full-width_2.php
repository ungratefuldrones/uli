<?php
/*
Template Name: Content: Full width_infographic
*/

	get_header();
?>

<div id="page-wrapper" style="background: url('/wp-content/themes/uli/assets/images/bg-placeholder.jpg'); background-size: cover;">
	<div class="contain">

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

		<!-- section header -->
		<header class="sub-header alternative">
			<h1 class="section-title"><?php the_title(); ?></h1>
			<span class="icon share share-this">Share</span>
		</header>
		<!-- end section header -->
		<!-- row -->
		<div class="row">
			<!-- column 1 -->
			<div class="full" >

				<div class="content_2">
				<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/uli_members_small_image_one.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:15%;" >
									<div class="info">
										<h3>33,000</h3>
									</div>
								<div class="side">
									<p style="width:100px;">ULI</br>Members</p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>in <span class="inner_font">68</span> countries</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/thirty_point_nine_four_smaill_image_2.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:10%;">
									<div class="info">
										<h3>3.94 MILLION</h3>
									</div>
								<div class="side">
									<p></p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Approximate value of volunteer hours donated by ULI members through their District and National Council</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/two_twenty_eight_governor_donations_small_image_3.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left: 26%;">
									<div class="info">
										<h3>288</h3>
									</div>
								<div class="side">
									<p>Governer</br>Donations</p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>ULI Foundation Governors who support ULI's mission philanthropic giving and the sharing their expertise across the entire organization</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/green_print_center_small_image_4.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:30%;" >
									<div class="info">
										<h3>44,028</h3>
									</div>
								<div class="side">
									<p></p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Equivalent number of cars taken off the road through reductions in greenhouse gas emission by properties in the ULI Greenprint Center for Building Performance</p>
				</div>
				<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/student_participants_small_image_5.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:20%;" >
									<div class="info">
										<h3>3,215</h3>
									</div>
								<div class="side">
									<p>Student</br>Participants</p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Student participants in UrbanPlan and the ULI Hines Competition</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/one_point_six_mill_small_image_6.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:8%;">
									<div class="info">
										<h3><span class="money">$</span>1.67 MILLION</h3>
									</div>
								<div class="side">
									<p></p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Amount raised through ULI member contributions to the Annual Fund and proceeds from the  ULI Foundation 2014 Fall Meeting Gala.</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/advisory_service_panels_small_image_7.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" >
									<div class="info">
										<h3>19</h3>
									</div>
								<div class="side">
									<p>Advisory Service</br>Panels</p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Advisory Services panels conveyed to assist communities with complex land use challenges, leveraging the expertise of more than <span class="inner_font">150</span> ULI member volunteers.</p>
				</div>
					<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/one_point_zero_three_small_image_8.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side" style="margin-left:12%;" >
									<div class="info">
										<h3>1.03 MILLION</h3>
									</div>
								<div class="side">
									<p></p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>Total users across the ULI family of websites</p>
				</div>
				<div class="inner_content_2" align="center">
				<img class="img" src="http://uli-annual-report.brandon.hfwebdev.com/wp-content/themes/uli/assets/images/social_media_followers_small_image_9.png">
					<div class="clear" style="clear:both;"></div>
							<div class="info_side"  style="margin-left:4%;side p:100px;">
									<div class="info">
										<h3>300,000</h3>
									</div>
								<div class="side" >
									<p style="width:100px;">Social Media</br>Followers</p>
								</div>
							</div>
						<div class="clear" style="clear:both;"></div>
								<p>People around the world reached by the ULI social media network</p>
				</div>
				<div class="clear" style="clear:both;"></div>
			</div>
			<!-- end column 1 -->
		</div>
		<!-- end row -->
		<?php endwhile; else : ?>
		<?php endif; ?>
	</div>
</div>

<?php get_footer(); ?>
			<style>
			p, h3
			{
					font-family:"Gotham A","Gotham B",sans-serif;
					color:#017573;
			}
			p:hover, h3:hover
			{
					color:#8FB14c;
			}
				.info h3
				{
					font-size:32px;
					margin:0px;
					padding:0px;
					font-weight:bold;
				}
				.side p
				{
					font-size:15px;
					font-weight:bold;
					margin-top:5px;
					margin-left:5px;
					padding:0px;
					text-align:left;
					line-height:12.5px;
				}
				.info, .side
				{
						float:left;
				}
				.img
				{
						width:221px;
						height:71px;

				}
				.content_2
				{
					width:85%;
					margin:0px auto;
				}
				.content_2 p
				{
					width:267px;
					font-size:12.5px;
				}
				.side p
				{
					width:120px;
				}
				.info_side
				{
					margin-left:17.5%;
				}
				.inner_content_2
				{
					float:left;
					margin:1%;
					width:280px;
					height:250px;
				}
				.money
				{	
					margin-top:-10px;
					font-size:15px;
				}
				.inner_font
				{
					font-weight:bold;
					font-size:25px;
				}
				@media only screen and (max-width: 900px)
				{
				.content_2
				{
					width:100%
				}
				}
				@media only screen and (max-width: 520px)
				{
				.content_2
				{
					width:100%
				}
				.inner_content_2
				{
					float:left;
					margin:0px;
				}
				}
			</style>