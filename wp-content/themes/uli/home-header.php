	<header id="home" class="hidden">

		<div class="contain">
			<!-- main logo -->
			<a class="logo" href="/"><?php echo get_bloginfo('name'); ?></a>
			<!-- end main logo -->
			<div class="click-nav"></div>
			<nav>
				<ul>
					<li class="social"><a target="_blank" href="<?php the_field('twitter', 'option'); ?>" class="icon twitter">Twitter</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('facebook', 'option'); ?>" class="icon facebook">Facbeook</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('linkedin', 'option'); ?>" class="icon linkedin">Linkedin</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('youtube', 'option'); ?>" class="icon instagram">Instagram</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('google', 'option'); ?>" class="icon google">Google +</a></li>
					<li class="social"><a target="_blank" href="<?php the_field('flickr', 'option'); ?>" class="icon flickr">Flickr</a></li>
					<li><a href="/about-us/uli/">About Us</a></li>
					<li class="last"><a href="<?php echo get_permalink( 40 ) ?>"><?php echo get_the_title( 40 ); ?> </a></li>
					<li class="social search">
						<form role="search" method="get" id="searchform" class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
							<input type="text" class="search" value="<?php echo get_search_query(); ?>" name="s" id="s" />
							<!-- <input type="submit" id="searchsubmit" value="<?php echo esc_attr_x( 'Search', 'submit button' ); ?>" /> -->
							<a href="#" class="icon search" id="search">Search</a>
						</form>

					</li>
				</ul>
			</nav>

		</div>
	</header>