<?php 
/*
Template Name: Homepage
*/

	get_header();

?>
<?php $custom_fields = get_post_custom(); ?>

<div class="page-background"></div>
<div id="video-wrapper">
	<div class="page-overlay"></div>
	<!-- full screen video -->
	<video autoplay poster="<?php echo get_bloginfo('template_url'); ?>/assets/images/Comp1.jpg" id="full-screen-video">
        <source src="<?php echo $custom_fields['home_video_url'][0] ?>" type="video/mp4">
        <source src="<?php echo get_bloginfo('template_url'); ?>/assets/video/iStock_000021019625_HD1080Video.ogg" type="video/ogv">
	</video>
	<!-- end full screen video -->
	<!-- full screen banner -->
	<article class="full banner hidden">
		<div class="contain">
			<?php /*the_field('home_banner_content'); */?>
            <div class="banner-content">
            	<h1>The <span>2014</span> ULI Annual Report</h1>
            	<h2>Developing <span>for</span> Humanity</h2>
            </div>
            <a class="brand-3 cta icon chat" id="launch-leadership" href="#leadership-message"><?php echo $custom_fields['home_content_button'][0]; ?></a>
            <a class="brand-2 cta icon explore" id="launch-report" href="#main-nav"><?php echo $custom_fields['home_navigation_button'][0]; ?></a>
		</div>
	</article>
	<!-- end full screen banner -->
	<div id="leadership-message">
		<!-- <div class="addthis_sharing_toolbox" data-title="ULI Annual Report: Leadership Message" data-url="<?php echo get_permalink(); ?>/#leadership-message"></div> -->
		<a class="icon overlay-close" alt="Back to the Home Page" title="Back to the Home Page">Back to Home</a>
		<a href="" class="back-to-top" alt="Back to the top" title="Back to the top"><img src="<?php echo get_bloginfo('template_url'); ?>/assets/images/BackToTop.png"></a>
		<section id="video">
			<div class="leadership-video">
				<iframe id="leadership-video-player-mobile" width="100%" height="360" src="https://www.youtube.com/embed/Yq9woKDsiag" frameborder="0" allowfullscreen></iframe>
				<div id="leadership-video-player"></div>
				
				<a href="" class="x" alt="close" title="close">Close</a>
			</div>
			<div class="video-overlay-bg" style="background-image: url('<?php echo get_bloginfo('template_url'); ?>/assets/images/leadership-team-back.jpg');"></div>
			<div class="video-overlay">
                <?php $post_message = get_post(429); ?>

                <?php echo $post_message->post_content ?>
                <div class="leader-circles-wrap">
                	<div class="title">Read Their Letters</div>
					<ul class="leaders">

						<?php

							// check if the repeater field has rows of data
							if( have_rows('section_leadership', 429) ):
								$count = 1;
							 	// loop through the rows of data
							    while ( have_rows('section_leadership', 429) ) : the_row();
									if( have_rows('leader') ):
								       	while ( have_rows('leader') ) : the_row(); ?>

								       		<li>
								       		<?php 
									       		$image = get_sub_field('leader_main_photo');
									       		//error_log(var_export($image,true));
									       		//print_r($image);
									       		$image_url = wp_get_attachment_thumb_url($image['id']);
								       		?>
						                        <div class="leader-link" data-hash="leader-<?php echo $count; ?>">
						                           	<div class="circle-hs" style="background-image: url('<?php echo $image_url; ?>');"></div>
						                           	<div class="text">
						                           		<span class="l-title"><?php the_sub_field('leader_name'); ?></span>
						                            	<span class="l-sub"><?php the_sub_field('leader_sub_title'); ?></span>
						                        	</div>
						                        </div>
						                    </li>
								       		
								   		<?php endwhile;
									else :
									    // no rows found
									endif;
									$count++;
							    endwhile;
							else :

							    // no rows found

							endif;

						?>
					</ul>
				</div>
				<div class="icon overlay-video-play" href="#">Play</div>
			</div>
			<!-- <div class="helper">Click Anywhere to Launch Video</div> -->
		</section>
		<section class="leader-wrap">

			<?php

				// check if the repeater field has rows of data
				if( have_rows('section_leadership', 429) ):
					$count = 1;
				 	// loop through the rows of data
				    while ( have_rows('section_leadership', 429) ) : the_row(); ?>

			    		<section class="leader leader-<?php echo $count; ?>">
			                <div class="contain">
			                	<div class="two-column-leader">

									<?php if( have_rows('leader') ):
								       	while ( have_rows('leader') ) : the_row(); ?>
					 						
					 						<?php 
									       		$image = get_sub_field('leader_main_photo');
									       		$image_url = wp_get_attachment_thumb_url($image['id']);
								       		?>

					                		<div class="column-leader">
							                    <img class="main-photo" src="<?php echo $image_url; ?>" alt="">
							                    <h2><?php the_sub_field('leader_name'); ?></h2>
							                    <h3><?php the_sub_field('leader_sub_title'); ?></h3>
							                </div>

										<?php endwhile;
									else :
									    // no rows found
									endif; ?>
								</div>
								<?php the_sub_field('leader_section_content'); ?>
								<div class="leader-sharing">
									<div class="addthis_sharing_toolbox" data-title="ULI Annual Report" data-url="<?php echo get_permalink(); ?>#leader-<?php echo $count; ?>"></div>
								</div>
							</div>
						</section>
					   	
						<?php $count++;
				    endwhile;
				else :

				    // no rows found

				endif;

			?>

	        	
	    </section>

	</div>
</div>

<?php get_footer(); ?>