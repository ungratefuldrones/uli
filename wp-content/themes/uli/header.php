<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<!--<meta name="viewport" content="width=device-width">-->

	<?php if ( is_front_page() ) { ?>
		<meta name="viewport" content="width=device-width">
	<?php } else { ?>
		<meta name="viewport" content="width=1024">
	<?php } ?>
	
	<meta property="og:title" content="<?php bloginfo('name'); ?>"/> 
	<meta property="og:description" content="<?php bloginfo('description'); ?>"/>
	<meta property="og:image" content="<?php bloginfo('template_url'); ?>/assets/images/fb-img.jpg"/>

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/styles.css" type="text/css" media="all" />
	<link rel="stylesheet" type="text/css" href="//cloud.typography.com/7324852/800906/css/fonts.css" />
	
</head>

<body <?php body_class(); ?>>

	<!-- main navigation -->
	<div id="main-nav" class="disabled">
		<header>
			<section class="inner-wrap">
				<div class="title" alt="Explore the 2014 Report" title="Explore the 2014 Report">Explore the 2014 Report</div>
				<ul>
					<li>
						<a href="/" alt="Back Home" title="Back Home">Back to Home</a>
					</li>
					<li>
						<a class="close-2 icon" id="close-main-nav" alt="Close" title="Close">Close</a>
					</li>
				</ul>
			</section>
		</header>
		<div class="mobile-title">ULI Priority Areas</div>
		<nav>
			<ul class="primary">
				<?php $numbers = array('one', 'two', 'three', 'four', 'five', 'six'); ?>
				<?php $mainMenuItems = get_field('main_menu_items', 'option'); ?>
				<?php for ($i=0; $i<3; $i++) { ?>
					<li class="disabled">
						<div class="background-color"></div>
						<div class="background-image" style="background: url('<?php echo get_field('story_navigation_background_image', $mainMenuItems[ $i ]->ID); ?>'); background-size: cover;"></div>
						<a href="<?php echo get_permalink($mainMenuItems[ $i ]->ID); ?>">
							<div class="number">ULI Priority <?php echo $numbers[$i]; ?></div>
							<div class="title"><?php echo $mainMenuItems[ $i ]->post_title; ?></div>
						</a>
					</li>
					<li class="disabled">
						<div class="background-color"></div>
						<div class="background-image" style="background: url('<?php echo get_field('story_navigation_background_image', $mainMenuItems[ $i + 3 ]->ID); ?>'); background-size: cover;"></div>
						<a href="<?php echo get_permalink($mainMenuItems[ $i + 3 ]->ID); ?>">
							<div class="number">ULI Priority <?php echo $numbers[$i + 3]; ?></div>
							<div class="title"><?php echo $mainMenuItems[ $i + 3 ]->post_title; ?></div>
						</a>
					</li>
				<?php } ?>
			</ul>
			<ul class="secondary">
				<li class="disabled">
					<div class="background-color"></div>
					<div class="link">
						<a href="/explore-fy14-highlights/">FY2014 Highlights</a>
					</div>
				</li>
				<li class="disabled">
					<div class="background-color"></div>
					<div class="link">
						<a href="/financial-performance-2/uli/balance-sheet/">Financial Performance</a>
					</div>
				</li>
				<li class="disabled last">
					<div class="background-color"></div>
					<div class="link">
						<a href="/about-us/videos/">Members on a Mission</a>
						<a href="/about-us/uli/">About Us</a>
						<a href="/about-us/contact/">Contact Us</a>
					</div>
				</li>
			</ul>
		</nav>
	</div>
	<!-- end main navigation -->

	<?php
		if ( is_front_page() ) {
			include_once('home-header.php');
		} else {
			include_once('global-header.php');
		}
	?>

